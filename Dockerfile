FROM clutteredcode/dind-compose
LABEL maintener Simon ROHART

RUN apk add --no-cache git
RUN apk add --no-cache bash
RUN apk add --no-cache nano

WORKDIR /docker-compose
