# Docker in docker

This project create a docker container with docker-compose inside.

## Prerequisites

You need docker and docker compose on your desktop

## Setup

Clone the project
```bash
git clone https://gitlab.com/icam-lille/dind.git myapp
```

Create .env file and customize it
```bash
cd myapp
cp .env.my_environment.dist > .env
```

Create docker-compose directory and put your project inside
```bash
git clone https://gitlab.myrepository.com/mygroup/myapp.git docker-compose
```

Create docker-in-docker container
```bash
docker-compose up -d
```

Connect to the container
```bash
docker-compose exec dind sh
```

Create sub-containers
```sh
docker-compose up -d
```
